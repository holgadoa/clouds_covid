# Covid-19 Web application
Description:
This repository contains all the code for the Clouds' Project "COVID-19 project" A web application that displays the covid19 virus evolution worldwide
the project is powered by Node.js and React


Developed by: Jose Luis Holgado


# Run
* cd into Clouds_Covid
* execute `npm start`

# Host

the App is hosted in Firebase under the url:
### `https://covid19-7059b.web.app/`


