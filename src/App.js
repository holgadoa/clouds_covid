import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import React, {Fragment, useEffect, useState} from "react";
import firebase from "firebase";
import { Menu, Paper } from "@material-ui/core";
import Navbar from "./components/Navbar";
import News from './components/News';
import CountryStats from "./components/CountryStats";
import Stats from "./components/Stats/Stats";



function renderCountry(routerProps){

  let countryId = routerProps.match.params.id;

  return (<CountryStats country={countryId} />)
}

function updateFavicon(image){
  const favicon = document.getElementById("favicon")
  favicon.href = image;
}

function App() {
  var storage = firebase.storage()
  var pathReference = storage.ref("");
  pathReference
      .child("coronavirus.png")
      .getDownloadURL()
      .then(function (url) {
        const favicon = document.getElementById("favicon")
        favicon.href = url;
      })
      .catch(function (error) {
        // Handle any errors
      });


  return (

    <div className="App">
      <Router>
        <Fragment>
          <div>
            <Paper elevation={0}>
              <Navbar />
              <Switch>
                <Route exact path="/" component={Stats} />
                <Route exact path='/News' component={News}/>
                <Route path = '/Country/:id' render = {routerProps => renderCountry(routerProps)} />
              </Switch>
            </Paper>
          </div>
        </Fragment>
      </Router>
    </div>

  );
}

export default App;
