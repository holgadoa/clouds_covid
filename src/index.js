import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import app from 'firebase/app';

app.initializeApp({
    apiKey: "AIzaSyAA58Nf5dLXU45WVO37k_odXyCgN4SvUOk",
    authDomain: "covid19-7059b.firebaseapp.com",
    databaseURL: "https://covid19-7059b-default-rtdb.firebaseio.com",
    projectId: "covid19-7059b",
    storageBucket: "covid19-7059b.appspot.com",
    messagingSenderId: "755177741878",
    appId: "1:755177741878:web:51e44d4b4b62a225bb0297",
    measurementId: "G-2G94TLZ6MT"
});


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
