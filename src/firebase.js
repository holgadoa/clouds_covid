import firebase from "firebase";
import "firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyAA58Nf5dLXU45WVO37k_odXyCgN4SvUOk",
    authDomain: "covid19-7059b.firebaseapp.com",
    databaseURL: "https://covid19-7059b-default-rtdb.firebaseio.com",
    projectId: "covid19-7059b",
    storageBucket: "covid19-7059b.appspot.com",
    messagingSenderId: "755177741878",
    appId: "1:755177741878:web:51e44d4b4b62a225bb0297",
    measurementId: "G-2G94TLZ6MT"
};

firebase.initializeApp(firebaseConfig);

export default firebase;