import { Button } from "@material-ui/core";
import React from "react";

export default function SignInButton({
  name,
  onclickFunction,
  fullWidth = null,
}) {
  return (
    <Button
      variant="contained"
      style={{
        textTransform: "none",
        backgroundColor: "#126eab",
        color: "white",
        padding: "3px 25px",
        margin: "5px",
      }}
      onClick={onclickFunction}
      fullWidth={fullWidth ? true : false}
    >
      {name}
    </Button>
  );
};


