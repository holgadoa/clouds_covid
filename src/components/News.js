import React, {useEffect, useState} from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import firebase from "firebase";


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function getList() {
  return fetch('https://api.covid19api.com/countries')
      .then(data => data.json())
}



export default function News() {
    const db = firebase.firestore();


    const updateDB = (user, news, date, country, doc) => {
        db.collection("news")
            .doc(doc)
            .set({
                user:user,
                content: news,
                date: date,
                country:country
            })
            .then(() => {
                console.log("DB updated");
            })
            .catch((error) => {
                console.log("error in updating DB");
            });
    };

    const checkDB = (doc) => {
        const collectionD = db.collection('news').doc(doc);
        const document = collectionD.get()
        if (!document.exists) {
            console.log('No such document!');
        } else {
            console.log('Document data:', document.data());
        }
    };
    var d = new Date();
    var year =d.getFullYear();
    var month =d.getMonth() + 1;
    var day =d.getDate();
  const classes = useStyles();
  const [state, setState] = React.useState({
    country: '',
    comment: "",
    surname:"",
    name: '',
    date: `${year}-${month}-${day}`,
  });

  const handleChange = (event) => {
    const name = event.target.name;
    setState({
      ...state,
      [name]: event.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault()
    if(state.country && state.comment && state.surname && state.name) {
        const today = parseInt(new Date().getTime());
        var doc = `news-${today}`
        updateDB(`${state.name}, ${state.surname}`,state.comment, state.date, state.country, doc);
        alert("The news were published")

    }
    else{alert("Please complete the form before submittion")}
  };

  const [CountriesList, setListCountries] = useState([]);

  useEffect(() => {
    let mounted = true;
    getList()
        .then(items => {
          if(mounted) {
            setListCountries(items)
          }
        })
    return () => mounted = false;
  }, [])


    const ref = firebase.firestore().collection('users');
    const [Eligible, setEligible] = useState(false);
    const [loading, setLoading] = useState(false);
    //REALTIME GET FUNCTION
    function getDocuments() {
        setLoading(true);
        ref
            .where('eligible', '==', true)
            .onSnapshot((querySnapshot) => {
                const items = [];
                querySnapshot.forEach((doc) => {
                    items.push(doc.id);
                });
                if(localStorage.getItem("user")){
                    var loggedUser = JSON.parse(localStorage.getItem("user")).email;
                    console.log(loggedUser,items);
                    var eligible = items.filter(item => item === loggedUser).length > 0;

                    setEligible(eligible);
                    setLoading(false);
                }

            });
    }

    useEffect(() => {
        getDocuments();
        // eslint-disable-next-line
    }, []);

  if(localStorage.getItem("user") && Eligible){

  return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Typography component="h1" variant="h5">
            Covid 19 News Posting
          </Typography>

          <form className={classes.form} noValidate>
            <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="Name"
                label="Name"
                name="Name"
                autoFocus
                onChange={handleChange}
                inputProps={{
                  name: 'name',

                }}
            />
            <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="SurName"
                label="SurName"
                name="SurName"
                autoFocus
                onChange={handleChange}
                inputProps={{
                  name: 'surname',

                }}
            />
            <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="Comment"
                label="Comment"
                id="Comment"
                onChange={handleChange}
                inputProps={{
                  name: 'comment',

                }}

            />
            <br />
            <br />
            <InputLabel htmlFor="age-native-simple">Country</InputLabel>

            <Select
                native
                value={state.country}
                onChange={handleChange}
                inputProps={{
                  name: 'country',
                  id: 'age-native-simple',
                }}
            >
              <option aria-label="None" value="" />
              <option value="worldwide">WorldWide</option>
              {CountriesList.map((row,idx) => (
                    <option value={row.Slug}>{row.Country}</option>
              ))}

            </Select>

            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={handleSubmit}
            >
              Insert News
            </Button>
            <Grid container>
              <Grid item xs>

              </Grid>
              <Grid item>

              </Grid>
            </Grid>


          </form>
        </div>
        <Box mt={8}>

        </Box>
      </Container>
  );}

  else if(localStorage.getItem("user") && !Eligible){
      return (
          <Container component="main" maxWidth="xs">
              <CssBaseline />
              <div className={classes.paper}>
                  <Typography component="h1" variant="h4">
                      Covid 19 News Posting
                  </Typography>

                  <Typography component="h1" variant="h5">
                      <br />
                      <br />
                      Sorry this area is restricted, ask administrator for support
                  </Typography>

              </div>
              <Box mt={8}>

              </Box>
          </Container>
      );
  }

  else{
    return (
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <div className={classes.paper}>
            <Typography component="h1" variant="h4">
              Covid 19 News Posting
            </Typography>

            <Typography component="h1" variant="h5">
              <br />
              <br />
              Please Sign in to post your news
            </Typography>

          </div>
          <Box mt={8}>

          </Box>
        </Container>
    );
  }
}