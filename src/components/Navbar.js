import {
  AppBar,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemText,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { useHistory } from "react-router-dom";
import React, { Fragment } from "react";
import MenuIcon from "@material-ui/icons/Menu";
import { useState } from "react";


function Navbar() {
  const [drawerOpen, setdrawerOpen] = useState(false);
  const history = useHistory();
  return (
    <Fragment>
      <AppBar position="sticky" style={{ backgroundColor: "#ab1212" }}>
        <Toolbar>
          <IconButton
            edge="start"
            style={{ color: "white" }}
            onClick={() => setdrawerOpen(true)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6">Covid 19 Worldwide tracker</Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        anchor="left"
        open={drawerOpen}
        onClose={() => setdrawerOpen(false)}
      >
        <List>
          {[
            {
              text: "Home",
              link: "/",
            },

            {
              text: "Add News",
              link: "/News",
            },
          ].map((item, index) => (
            <ListItem
              button
              key={item.text}
              onClick={() => {
                history.push(item.link);
                setdrawerOpen(false)
              }}
            >
              <ListItemText primary={item.text} />
            </ListItem>
          ))}
        </List>
      </Drawer>
    </Fragment>
  );
}

export default Navbar;
