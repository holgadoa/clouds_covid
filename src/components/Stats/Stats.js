import React, {useEffect, useState} from 'react';
import {
    Card,
    CardContent,
} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import {AreaChart, Area, CartesianGrid,Bar,BarChart,Legend, XAxis, YAxis, Tooltip, PieChart, Pie, Cell } from 'recharts';
import Paper from '@material-ui/core/Paper';
import TableCell from '@material-ui/core/TableCell';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import firebase from "firebase";
import Button from "../layout/Button";
import LogOutButton from "../layout/LogOutButton";
import { useHistory } from "react-router-dom";
import {
    Typography
} from "@material-ui/core";
import PropTypes from "prop-types";


export const signInWithGoogle = (auth, googleProvider) => {
    auth.signInWithPopup(googleProvider).then((res) => {
        sessionStorage.setItem("user", res.user.email)
        localStorage.setItem('user', JSON.stringify(res.user));

    }).catch((error) => {
        console.log(error.message)
    })
}
export const signOutWithGoogle = (auth) => {

    auth.signOut().then(() => {
        localStorage.removeItem('user');
        sessionStorage.removeItem("user")
        // Sign-out successful.
    }).catch((error) => {
        console.log(error)
        // An error happened.
    })
}


const COLORS = ['#e78098', '#3cbaff', '#FFBB28'];


function dataFormat(date, Recovered, Deaths, Cases) {
    return {date, Recovered, Deaths, Cases};
}

function summaryCovidData(Dates, Recovered, Deaths, Cases){
    var covidSummaryArray = []
    for (var i = 0; i < Dates.length; i++)
    {covidSummaryArray.push(dataFormat(Dates[i], Recovered[i], Deaths[i], Cases[i]))}
return covidSummaryArray}

function AjustCases(Dates, Recovered, Deaths, Cases)
{
    var newList = [];
    for(var i = Dates.length - 8; i < Dates.length; i++)
    {
        var death =Deaths[i] - Deaths[i -1];
        var recovered =Recovered[i] - Recovered[i -1];
        var cases = Cases[i] - Cases[i -1];
        newList.push(dataFormat(Dates[i],recovered,death,cases))
    }

    return newList

}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        height: 340,
        width: 300,
    },

}));

////////////////////////////////////////////////////////
function createDataCountry(Slug,country, newCases, totalCases, newRecoveries, totalRecoveries, NewDeaths, TotalDeaths) {
    return {Slug,country, newCases, totalCases, newRecoveries, totalRecoveries, NewDeaths, TotalDeaths};
}



function loadDataAllCountries(data){


    const totalCases = data.TotalConfirmed;
    return createDataCountry(data.Slug,data.Country,data.NewConfirmed, totalCases, data.NewRecovered, data.TotalRecovered, data.NewDeaths, data.TotalDeaths)
}




function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function getComparator(order, orderBy) {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}

const headCells = [
    { id: 'country', numeric: false, disablePadding: false, label: 'Country' ,style:"grey"},
    { id: 'newCases', numeric: true, disablePadding: false, label: 'new Cases',style:"lightyellow" },
    { id: 'totalCases', numeric: true, disablePadding: false, label: 'total Cases' ,style:"lightyellow"},
    { id: 'newRecoveries', numeric: true, disablePadding: false, label: 'new Recoveries',style:"#80b8e7" },
    { id: 'totalRecoveries', numeric: true, disablePadding: false, label: 'total Recoveries',style:"#80b8e7" },
    { id: 'NewDeaths', numeric: true, disablePadding: false, label: 'New Deaths' ,style:"#e7808c"},
    { id: 'TotalDeaths', numeric: true, disablePadding: false, label: 'Total Deaths',style:"#e7808c"}
];

function toogleButtonSignGoogle(Buttonstate, auth, googleProvider)
{   Buttonstate(true)
    return signInWithGoogle(auth, googleProvider)}

function toogleButtonSignOutGoogle(Buttonstate, auth)
{   Buttonstate(false)
    return signOutWithGoogle(auth)}

function SingIn(setButton, Buttonstate, auth, googleProvider) {
    const history = useHistory();

    if(!setButton){
        return (
        <div align={"center"}>
            <Button
                name={"Sign In"}

                onclickFunction={() => {toogleButtonSignGoogle(Buttonstate, auth, googleProvider)}}
            />

            <Button
                name={"Add News"}
                onclickFunction={() => history.push("/News")}
            />

        </div>
    );}
    else{return (
        <div align={"center"}>
            <LogOutButton
                name={"Log Out"}
                onclickFunction={() => {toogleButtonSignOutGoogle(Buttonstate, auth, googleProvider)}}
            />

            <Button
                name={"Add News"}
                onclickFunction={() => history.push("/News")}
            />

        </div>);}

};

function EnhancedTableHead(props) {
    const { classes, order, orderBy, onRequestSort } = props;
    const createSortHandler = (property) => (event) => {
        onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow>
                {headCells.map((headCell) => (
                    <TableCell

                        key={headCell.id}
                        align={'right'}
                        padding={headCell.disablePadding ? 'none' : 'default'}
                        sortDirection={orderBy === headCell.id ? order : false}
                        style={{background:headCell.style }}

                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={createSortHandler(headCell.id)}
                        >
                            {headCell.label}
                            {orderBy === headCell.id ? (
                                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
                            ) : null}
                        </TableSortLabel>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};



const useStyles2 = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 750,
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
}));

//////////////insertion/////
function selectColor(id)
{   var color = "lightyellow";
    if(id > 2)
    {color = "#80b8e7";}
    if(id > 5)
    {color = "#dc7777";}
    return {
        backgroundColor: color
    }
}



function loadDataGlobal(data){
    const totalCases = data.TotalConfirmed;
    const recoveryRate = (data.TotalRecovered / totalCases)*100;
    const deathRate = (data.TotalDeaths / totalCases)*100;
    const ActiveCases = data.TotalConfirmed - data.TotalDeaths- data.TotalRecovered;

    const world = [{key:"Total Cases",value: data.TotalConfirmed},
        {key:"New Cases",value:data.NewConfirmed},
        {key:"Active Cases",value:ActiveCases},
        {key:"totalRecoveries",value:data.TotalRecovered},
        {key:"newRecoveries",value:data.NewRecovered},
        {key:"Recovery Rate",value:recoveryRate},
        {key:"totalDeaths",value:data.TotalDeaths},
        {key:"newDeaths",value:data.NewDeaths},
        {key:"MortalityRate",value:deathRate}];
    return world
}

function loadDataGlobalReduce(data){

    const ActiveCases = data.TotalConfirmed - data.TotalDeaths- data.TotalRecovered;

    const reduce = [
        { name: 'Dead Cases', value: data.TotalDeaths },{ name: 'Recovered Cases', value: data.TotalRecovered }, { name: 'Active Cases', value: ActiveCases }
    ];
    return reduce
}

function getList() {
    return fetch('https://api.covid19api.com/summary')
        .then(data => data.json())
}

function getUrl2() {
    return fetch('https://corona.lmao.ninja/v2/historical/all')
        .then(data => data.json())
}






export default function Stats() {
    var storage = firebase.storage()
    const auth = firebase.auth();
    const googleProvider = new firebase.auth.GoogleAuthProvider()
    const [image, setImage] = useState("");
    const [world, setList] = useState([]);
    const [worldReduced, setListReduced] = useState([]);
    const [CountriesList, setListCountries] = useState([]);




    const [CasesList, setCasesList] = useState([]);
    const [FullCasesList, setFullCasesList] = useState([]);


    useEffect(() => {
        let mounted = true;
        getUrl2()
            .then(items => {
                if(mounted) {

                    setCasesList(items)


                    const dates = Object.keys(items["cases"]);
                    const cases =Object.values(items["cases"]);
                    const recoveries =Object.values(items["recovered"]);
                    const deaths =Object.values(items["deaths"]);
                    const allCases = summaryCovidData(dates, recoveries, deaths, cases)
                    const weekCases = AjustCases(dates, recoveries, deaths, cases)
                    setCasesList(weekCases)
                    setFullCasesList(allCases)



                }
            })
        return () => mounted = false;
    }, [])

    var pathReference = storage.ref("");
    pathReference
        .child("coronavirus.png")
        .getDownloadURL()
        .then(function (url) {

            setImage(url);
        })
        .catch(function (error) {
            // Handle any errors
        });

    const classes2 = useStyles2();
    const classes = useStyles();
    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('country');
    const [setButton, Buttonstate] = React.useState(!(localStorage.getItem("user") === null));


    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };


    const ref = firebase.firestore().collection('news');
    const [CountryList, setCountry] = useState([]);
    const [loading, setLoading] = useState(false);
    //REALTIME GET FUNCTION
    function getDocuments() {
        setLoading(true);
        ref
            .where('country', '==', "worldwide")
            .onSnapshot((querySnapshot) => {
                const items = [];
                querySnapshot.forEach((doc) => {
                    items.push(doc.data());
                });
                setCountry(items);
                setLoading(false);
            });
    }

    useEffect(() => {
        getDocuments();
        // eslint-disable-next-line
    }, []);
    const db = firebase.firestore();
    const writeDB = (dataToUPload, Newdate, document) => {
        db.collection("countrySummary")
            .doc(document)
            .set({
                content: dataToUPload,
                date: Newdate,
            })
            .then(() => {
                console.log("writing into DB ");
            })
            .catch((error) => {
                console.log("error in updating DB");
            });
    };

    const ref2 = firebase.firestore().collection('countrySummary').doc("worldwide");

    useEffect(async () => {
        try {
            ref2.get().then(async (doc) => {
                const latedate = parseInt(new Date().getTime() / 86400000);

                // Check if document is updated
                if (doc.exists) {
                    console.log("document exist in database")
                    const date = doc.data().date;
                    //Checking if the data at the DB is up to date
                    if (latedate > date) {

                        await getList().then(data => {
                            console.log(data)
                            console.log(data.Global)
                            setListCountries(data.Countries.map(x =>loadDataAllCountries(x)))
                            setList(loadDataGlobal(data.Global))
                            setListReduced(loadDataGlobalReduce(data.Global))

                            writeDB(data, latedate, "worldwide")
                        })

                    } else {
                        //When the data is  updated retrieve DB data
                        console.log("dumped from DB");
                        var dataDic = doc.data().content
                        setListCountries(dataDic.Countries.map(x =>loadDataAllCountries(x)))
                        setList(loadDataGlobal(dataDic.Global))
                        setListReduced(loadDataGlobalReduce(dataDic.Global))

                    }
                } else {
                    //Create new document with the country data into de DB
                    await getList().then(data =>{
                        console.log(data.Global)
                        setListCountries(data.Countries.map(x =>loadDataAllCountries(x)))
                        setList(loadDataGlobal(data.Global))
                        setListReduced(loadDataGlobalReduce(data.Global))

                        writeDB(data, latedate, "worldwide")
                    })

                }
            });
        } catch (error) {
            console.log(error);
        }
    }, []);

    return (
        <Grid container className={classes.root} spacing={2}>
            <Grid item xs={12}>
                <Grid container direction="column"  justify="flex-start" alignItems="center" spacing={5}>
                    <Grid key={0} item>
                        <Typography variant="h2" align={"center"} style={{ padding: "30px 15px" }}>
                            <img src={ image } style={{width:100, hight:"auto"}} alt="corona"/>
                        Covid-19</Typography>
                        <Typography variant="h2" style={{ padding: "30px 15px" }}>
                            Live Updates and Statistics
                            <br />
                        </Typography>

                        <Typography variant="h4"  align="center" >
                            Home Page
                            <br />
                            <br />
                        </Typography>

                        {SingIn(setButton, Buttonstate, auth, googleProvider)}

                    </Grid>
                        <Grid key={1} item>
                            <Typography variant="h4"  align="center" >
                                <br />
                                <br />
                                WorlWide Covid Report
                                <br />
                                <br />
                            </Typography>

                            <Paper elevation={3} >
                                <Table className={classes.table} aria-label="Worldwide-summary">
                                    <TableHead>
                                        <TableRow style={{backgroundColor:'lightgrey'}}>
                                            <TableCell align="center" colSpan={2}>Corona Virus Summary Worldwide</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {world.map((row,idx) => (
                                            <TableRow style={selectColor(idx)}>
                                                <TableCell align="left" >{row.key}</TableCell>

                                                <TableCell align="left">{row.value}</TableCell>
                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>

                            </Paper>

                        </Grid>

                    <Grid key={2} item>
                        <Typography variant="h4"  align="center" >
                            Corana Virus Cases Distribution Worldwide
                            <br />
                            <br />
                        </Typography>
                        <PieChart width={800} height={500}>
                            <Tooltip />

                            <Pie
                                data={worldReduced}
                                cx={300}
                                cy={200}
                                outerRadius={180}
                                fill="#8884d8"
                            >
                                {
                                    worldReduced.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
                                }
                            </Pie>
                            <Legend layout="vetical" verticalAlign="middle" align="right"/>
                        </PieChart>

                    </Grid>


                    <Grid key={3} item>
                        <Typography variant="h4"  align="center" >
                            Daily Corona Virus Cases Worldwide
                            <br />
                            <br />
                        </Typography>
                        <BarChart width={730} height={250} data={CasesList}
                                  margin={{top: 10, right: 30, left: 50, bottom: 0}}>
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="date" />
                            <YAxis />
                            <Tooltip />
                            <Legend iconSize={10} width={0} height={280} />
                            <Bar dataKey="Deaths" fill={COLORS[0]} />
                            <Bar dataKey="Recovered" fill={COLORS[1]} />
                            <Bar dataKey="Cases" fill={COLORS[2]} />
                        </BarChart>

                    </Grid>

                    <Grid key={4} item>
                        <Typography  variant="h4"  align="center">
                            <br />
                            Total Corona Virus Cases Worldwide
                            <br />

                        </Typography>

                        <AreaChart width={700} height={400} data={FullCasesList}
                                   margin={{top: 10, right: 30, left: 50, bottom: 0}}>
                            <CartesianGrid strokeDasharray="3 3"/>
                            <XAxis dataKey="date"/>
                            <YAxis/>
                            <Tooltip/>
                            <Area type='monotone' dataKey='Deaths' stackId="1" stroke={COLORS[0]} fill={COLORS[0]} />
                            <Area type='monotone' dataKey='Recovered' stackId="1" stroke={COLORS[1]} fill={COLORS[1]} />
                            <Area type='monotone' dataKey='Cases' stackId="1" stroke={COLORS[2]} fill={COLORS[2]} />

                            <Legend layout="horizontal" verticalAlign="top" />
                        </AreaChart>
                    </Grid>
                    <Grid key={5} item>
                        <Typography variant="h4"  align="center" >
                            <br />
                            Corona Virus Cases by Country
                            <br />
                            <br />
                        </Typography>
                        <Table
                            className={classes2.table}
                        >
                            <EnhancedTableHead
                                classes={classes2}
                                order={order}
                                orderBy={orderBy}
                                onRequestSort={handleRequestSort}

                            />
                            <TableBody>
                                {stableSort(CountriesList, getComparator(order, orderBy))
                                    .map((row) => {

                                        return (
                                            <TableRow
                                                hover
                                                tabIndex={-1}
                                                key={row.country}

                                            >
                                                <TableCell style={{background:"grey"}} component="a" href={`/Country/${row.Slug}`} align="right">{row.country}</TableCell>
                                                <TableCell style={{background:"lightyellow"}} align="right">{row.newCases}</TableCell>
                                                <TableCell style={{background:"lightyellow"}} align="right">{row.totalCases}</TableCell>
                                                <TableCell style={{background:"#80b8e7"}} align="right">{row.newRecoveries}</TableCell>
                                                <TableCell style={{background:"#80b8e7"}} align="right">{row.totalRecoveries}</TableCell>
                                                <TableCell style={{background:"#e7808c"}} align="right">{row.NewDeaths}</TableCell>
                                                <TableCell style={{background:"#e7808c"}} align="right">{row.TotalDeaths}</TableCell>
                                            </TableRow>
                                        );
                                    })}

                            </TableBody>
                        </Table>
                    </Grid>

                    <Grid key={6} item>
                        <Typography variant="h2" align="center">
                            <br />
                            Global News Board
                            <br />
                            <br />
                        </Typography>
                        {CountryList.map((msg, index) => {
                            return (
                                <Card
                                >
                                    <CardContent>
                                    <p>
                                        User: {msg.user}
                                    </p>
                                        <p>
                                           Country: {msg.country}
                                        </p>
                                        <p>
                                           News: {msg.content}
                                        </p>
                                        <p style={{ textAlign: "left", color: "gray" }}>
                                           Date: {msg.date}
                                        </p>

                                    </CardContent>
                                </Card>
                            );
                        })}


                    </Grid>

                    <footer align="center">
                        <hr/>
                        <h4>Data Source:</h4>
                        <p><a href="https://covid19api.com/">COVID-19 API / John Hopkins CSSE</a></p>
                    </footer>

                </Grid>
            </Grid>


        </Grid>

    );

}



