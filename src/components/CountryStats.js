import React, {useEffect, useState} from 'react'
import Grid from "@material-ui/core/Grid";
import {Card, CardContent, Typography} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {
    Area,
    AreaChart,
    Bar,
    BarChart,
    CartesianGrid,
    Cell,
    Legend,
    Pie,
    PieChart,
    Tooltip,
    XAxis,
    YAxis
} from "recharts";
import {makeStyles} from "@material-ui/core/styles";

import firebase from "firebase";
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        height: 340,
        width: 300,
    },

}));

const data2 =  [
    {name: 'Page A', uv: 4000, pv: 2400, amt: 2400},
    {name: 'Page B', uv: 3000, pv: 1398, amt: 2210},
    {name: 'Page C', uv: 2000, pv: 9800, amt: 2290},
    {name: 'Page D', uv: 2780, pv: 3908, amt: 2000},
    {name: 'Page E', uv: 1890, pv: 4800, amt: 2181},
    {name: 'Page F', uv: 2390, pv: 3800, amt: 2500},
    {name: 'Page G', uv: 3490, pv: 4300, amt: 2100},
];


function selectColor(id)
{   var color = "lightyellow";
    if(id > 2)
    {color = "#80b8e7";}
    if(id > 5)
    {color = "#dc7777";}
    return {
        backgroundColor: color
    }
}


const COLORS = ['#e78098', '#3cbaff', '#FFBB28'];

function getList() {
    return fetch('https://api.covid19api.com/summary')
        .then(data => data.json())
}

function getUrl2(countrySlug) {
    var d = new Date();
    var year =d.getFullYear();
    var month =d.getMonth() + 1;
    var day =d.getDate();
    let URLSLUG =`https://api.covid19api.com/total/country/${countrySlug}?from=2020-04-01T00:00:00Z&to=${year}-${month}-${day}T00:00:00Z`;
    return fetch(URLSLUG)
        .then(data => data.json())
}

function loadDataGlobal(data){
    const totalCases = data.TotalConfirmed;
    const recoveryRate = (data.TotalRecovered / totalCases)*100;
    const deathRate = (data.TotalDeaths / totalCases)*100;
    const ActiveCases = data.TotalConfirmed - data.TotalDeaths- data.TotalRecovered;

    const world = [{key:"Total Cases",value: data.TotalConfirmed},
        {key:"New Cases",value:data.NewConfirmed},
        {key:"Active Cases",value:ActiveCases},
        {key:"totalRecoveries",value:data.TotalRecovered},
        {key:"newRecoveries",value:data.NewRecovered},
        {key:"Recovery Rate",value:recoveryRate},
        {key:"totalDeaths",value:data.TotalDeaths},
        {key:"newDeaths",value:data.NewDeaths},
        {key:"MortalityRate",value:deathRate}];
    return world
}

function loadDataGlobalReduce(data){

    const ActiveCases = data.TotalConfirmed - data.TotalDeaths- data.TotalRecovered;

    const reduce = [
        { name: 'Dead Cases', value: data.TotalDeaths },{ name: 'Recovered Cases', value: data.TotalRecovered }, { name: 'Active Cases', value: ActiveCases }
    ];
    return reduce
}

export default function CountryStats({country}) {

    const ref = firebase.firestore().collection('news');
    const classes = useStyles();
    const [world, setList] = useState([]);
    const [worldReduced, setListReduced] = useState([]);
    const [CountryList, setCountry] = useState([]);
    const [items2, setItems] = useState([]);
    const [items3, setItemsReduced] = useState([]);

    const [nameCountry, setName] = useState([]);
    const [loading, setLoading] = useState(false);

    const db = firebase.firestore();
    const writeDB = (dataToUPload, Newdate, document) => {
        db.collection("countrySummary")
            .doc(document)
            .set({
                content: dataToUPload,
                date: Newdate,
            })
            .then(() => {
                console.log("writing into DB ");
            })
            .catch((error) => {
                console.log("error in updating DB");
            });
    };

    const ref2 = firebase.firestore().collection('countrySummary').doc(country);

    useEffect(async () => {
        try {
            ref2.get().then(async (doc) => {
                const today = parseInt(new Date().getTime() / 86400000);

                // Check if document is updated
                if (doc.exists) {
                    console.log("document exist")
                    const date = doc.data().date;
                    //Checking if the data is up to date
                    if (today > date) {
                        //When the data is not updated, push new version from API to DB and retrieve API data
                        await getList().then(data => {
                            console.log(data)

                            const selectedCountry = data.Countries.filter(x => x.Slug===country)[0]
                            setName(selectedCountry.Country)
                            setList(loadDataGlobal(selectedCountry));
                            setListReduced(loadDataGlobalReduce(selectedCountry));

                            writeDB(selectedCountry, today, country)
                        })

                    } else {
                        //When the data is  updated retrieve DB data
                        console.log("dumped from DB");
                        var dataDic = doc.data().content
                        setName(dataDic.Country)
                        setList(loadDataGlobal(dataDic));
                        setListReduced(loadDataGlobalReduce(dataDic));
                    }
                } else {
                    //Create new document with the country data into de DB
                    await getList().then(data =>{
                        const selectedCountry = data.Countries.filter(x => x.Slug===country)[0]
                        setName(selectedCountry.Country)
                        setList(loadDataGlobal(selectedCountry));
                        setListReduced(loadDataGlobalReduce(selectedCountry));

                        writeDB(selectedCountry, today, country)
                    })

                }
            });
        } catch (error) {
            console.log(error);
        }
    }, []);

    function dataFormat(Date, Recovered, Deaths, Confirmed) {
        return {Date, Recovered, Deaths, Confirmed};
    }


    function AjustCases(Data)
    {
        var newList = [];
        for(var i = Data.length - 7; i < Data.length; i++)
        {
            var death =Data[i].Deaths - Data[i-1].Deaths;
            var recovered = Data[i].Recovered - Data[i-1].Recovered;
            var cases = Data[i].Confirmed - Data[i-1].Confirmed;
            newList.push(dataFormat(Data[i].Date,recovered,death,cases))

        }

        return newList

    }

    useEffect(() => {
        let mounted = true;
        getUrl2(country)
            .then(items => {
                if(mounted) {
                    console.log(items)
                    setItems(items);
                    setItemsReduced(AjustCases(items))

                }
            });
        return () => mounted = false;
    }, [])

    //REALTIME GET FUNCTION
    function getNews() {
        setLoading(true);
        ref
            .where('country', '==', country)

            .onSnapshot((querySnapshot) => {
                const items = [];
                querySnapshot.forEach((doc) => {
                    items.push(doc.data());
                });
                setCountry(items);
                setLoading(false);
            });
    }

    useEffect(() => {
        getNews();
        // eslint-disable-next-line
    }, []);


    return (
        <Grid container className={classes.root} spacing={2}>
            <Grid item xs={12}>
                <Grid container direction="column"  justify="flex-start" alignItems="center" spacing={2}>

                    <Grid key={0} item>
                        <Typography variant="h2" style={{ padding: "30px 15px" }}>
                            Live Updates and Statistics in {nameCountry}

                            <br />
                        </Typography>

                        <Paper elevation={3} >
                            <Table className={classes.table} aria-label="Worldwide-summary">
                                <TableHead>
                                    <TableRow style={{backgroundColor:'lightgrey'}}>
                                        <TableCell align="center" colSpan={2}>Corona Virus Summary in {nameCountry}</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {world.map((row,idx) => (
                                        <TableRow style={selectColor(idx)}>
                                            <TableCell align="left" >{row.key}</TableCell>
                                            <TableCell align="left">{row.value}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>

                        </Paper>

                    </Grid>

                    <Grid key={1} item>
                        <Typography variant="h4"  align="center" >
                            Corona Virus Cases Distribution in {nameCountry}
                            <br />
                            <br />
                        </Typography>
                        <PieChart width={800} height={500}
                                  margin={{top: 10, right: 30, left: 150, bottom: 0}}>
                            <Tooltip />

                            <Pie
                                data={worldReduced}
                                cx={300}
                                cy={200}
                                outerRadius={210}
                                fill="#8884d8"
                            >
                                {
                                    worldReduced.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
                                }
                            </Pie>
                            <Legend />
                        </PieChart>

                    </Grid>


                    <Grid key={2} item>
                        <Typography variant="h4"  align="center" >
                            Daily Corona Virus Cases {nameCountry}
                            <br />
                            <br />
                        </Typography>
                        <BarChart width={830} height={350} data={items3}
                                  margin={{top: 10, right: 30, left: 50, bottom: 0}}>
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="Date" />
                            <YAxis />
                            <Tooltip />
                            <Legend iconSize={10} width={0} height={380} />
                            <Bar dataKey="Deaths" fill={COLORS[0]} />
                            <Bar dataKey="Recovered" fill={COLORS[1]} />
                            <Bar dataKey="Confirmed" fill={COLORS[2]} />
                        </BarChart>

                    </Grid>

                    <Grid key={3} item>
                        <Typography  variant="h4"  align="center">
                            <br />
                            Total Corona Virus Cases in {nameCountry}
                            <br />
                            <br />

                        </Typography>

                        <AreaChart width={800} height={400} data={items2}
                                   margin={{top: 10, right: 30, left: 50, bottom: 0}}>
                            <CartesianGrid strokeDasharray="3 3"/>
                            <XAxis dataKey="Date"/>
                            <YAxis/>
                            <Tooltip/>
                            <Area type='monotone' dataKey='Deaths' stackId="1" stroke={COLORS[0]} fill={COLORS[0]} />
                            <Area type='monotone' dataKey='Recovered' stackId="1" stroke={COLORS[1]} fill={COLORS[1]} />
                            <Area type='monotone' dataKey='Confirmed' stackId="1" stroke={COLORS[2]} fill={COLORS[2]} />
                            <Legend iconSize={10} width={0} height={410} />
                        </AreaChart>
                    </Grid>


                    <Grid key={4} item>
                        <Typography variant="h2" align="center">
                            <br />
                            Global News Board
                            <br />
                            <br />
                        </Typography>
                        {CountryList.map((msg, index) => {
                            return (
                                <Card
                                >
                                    <CardContent>
                                        <p>
                                            User: {msg.user}
                                        </p>
                                        <p>
                                            Country: {msg.country}
                                        </p>
                                        <p>
                                            News: {msg.content}
                                        </p>
                                        <p style={{ textAlign: "left", color: "gray" }}>
                                            Date: {msg.date}
                                        </p>

                                    </CardContent>
                                </Card>
                            );
                        })}


                    </Grid>
                    <footer align="center">
                        <hr/>
                        <h4>Data Source:</h4>
                        <p><a href="https://covid19api.com/">COVID-19 API / John Hopkins CSSE</a></p>
                    </footer>

                </Grid>
            </Grid>

        </Grid>
    );

}

